import React from 'react';
import utils from './utils';
import classNames from 'classnames';
import ComposedComponent from './ComposedComponent';

import Radio, { RadioGroup } from 'material-ui/Radio';
import {FormLabel, FormControl, FormControlLabel } from 'material-ui/Form';

class Radios extends React.Component {

    render() {
        let items = this.props.form.titleMap.map(function(item, index) {
            return (
                <FormControlLabel key={index} value={item.value} disabled={this.props.form.readonly} control={<Radio />} label={item.name} />
            )
        }.bind(this));

        return (
            <span className={this.props.form.htmlClass}>
                <FormControl required >
                <FormLabel component="legend">{this.props.form.title}</FormLabel>
                <RadioGroup
                    value={this.props.policy.general.design}
                    onChange={this.props.onChangeValidate}
                >
                  {items}
                </RadioGroup>
                </FormControl>
            </span>
        );
    }
}

export default ComposedComponent(Radios);
